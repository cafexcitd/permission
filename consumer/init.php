<?php

// configure the JSON to use in the session.
$json = '
    {
        "webAppId": "5d00b627-c89a-414c-9bdf-224af7f3f303",
        "allowedOrigins": ["*"],
        "urlSchemeDetails": {
            "host": "192.168.4.21",
            "port": "8080",
            "secure": false
        },
        "voice":
        {
            "username": "assist-555123451",
            "displayName": "Priti",
            "domain": "192.168.4.21",
            "inboundCallingEnabled": false
        },
        "additionalAttributes":
        {
            "AED2.metadata": {
            "role": "agent",
            "name": "priti",
            "permissions": {
                "viewable": ["test"],
                "interactive": ["go", "text", "default"]
            }
        },
            "AED2.allowedTopic": "%s"
        }
    }
';

// determine some corellation ID, and add it to the provisioning JSON
$corellationID = 'assist-555123451';
$json = sprintf($json, $corellationID);

// configure the curl options
$ch = curl_init("http://192.168.4.21:8080/gateway/sessions/session");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
curl_setopt($ch, CURLOPT_HTTPHEADER, array(         
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json))
);

// execute HTTP POST & close the connection
$response = curl_exec($ch);
curl_close($ch);

// add CORS header - not necessary for Assist
header("Access-Control-Allow-Origin: *");

// Notice that in this example - we are returning the corellationID rather
// than the sessionID! This assumes that the consumer client code does not
// already know the corellation ID!
echo $corellationID;