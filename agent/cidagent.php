<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://192.168.4.21:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.4.21:8443/assistserver/sdk/web/shared/css/shared-window.css">

    <style type="text/css">

        #share {
            border: 1px solid grey;
        }

        #share {
            width: 1080px;
            height: 640px;
            position: relative;
        }

    </style>
</head>

<body>

<!-- remote screen share -->
<div id="share"></div>


<!-- request the consumer allow screen share -->
<button id="request-share">Request screen share</button>

<!-- button through which agent clicks to end call -->
<button id="end">End</button>


<h3>Mode</h3>
<form class="mode">
    <input type="radio" name="mode" value="control" checked>Control<br>
    <input type="radio" name="mode" value="draw">Draw<br>
    <input type="radio" name="mode" value="spotlight">Spotlight<br>
</form>

<h3>Annotation Control</h3>
<form class="annotation-control">
    <input type="color" class="color" value="#ff0000">
    <input type="text" class="opacity" value="0.5">
    <input type="text" class="width" value="2">
    <button type="submit">Update</button>
    <button id="clear">Clear</button>
</form>

<!-- libraries needed for Assist SDK -->
<script src="https://192.168.4.21:8443/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
<script src="https://192.168.4.21:8443/gateway/adapter.js"></script>
<script src="https://192.168.4.21:8443/gateway/csdk-sdk.js"></script>
<script src="https://192.168.4.21:8443/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="https://192.168.4.21:8443/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="https://192.168.4.21:8443/assistserver/sdk/web/agent/js/assist-console.js"></script>

<!-- load jQuery - helpful for DOM manipulation -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- control -->
<script>

    <?php
    // since we know that the agent will require to be provisioned, we
    // can eagerly provision a session token on the page load rather
    // than do so lazily later

    // include the Provisioner class we created to do the provisioning
    require_once('Provisioner.php');
    $provisioner = new Provisioner;
    $token = $provisioner->provisionAgent(
        'agent1',
        '192.168.4.21',
        '.*');
    ?>

    var sessionId = '<?php echo $token; ?>';


    var QueryString = function () {
                 // This function is anonymous, is executed immediately and
                 // the return value is assigned to QueryString!
                 var query_string = {};
                 var query = window.location.search.substring(1);
                 var vars = query.split("&");
                 for (var i = 0; i < vars.length; i++) {
                     var pair = vars[i].split("=");
                     // If first entry with this name
                     if (typeof query_string[pair[0]] === "undefined") {
                         query_string[pair[0]] = pair[1];
                         // If second entry with this name
                     } else if (typeof query_string[pair[0]] === "string") {
                         var arr = [query_string[pair[0]], pair[1]];
                         query_string[pair[0]] = arr;
                         // If third or later entry with this name
                     } else {
                         query_string[pair[0]].push(pair[1]);
                     }
                 }
                 return query_string;
             } ();

  
     
     
             var myCid = QueryString.cid;


    var AgentModule = function (sessionId) {
        // Declare variables needed during the call
        var
        // native DOM elements
            share,
        // jQuery elements for easier event binding
            $end,
            $requestShare,
            $mode,
            $annotationControl;


        // organise functions to do important tasks...
        cacheDom(); // cache the DOM elements we will need later during the call
        setClickHandlers();  // define what happens when our app's buttons are clicked
        bindAgentSdkCallbacks();  // set all of the agentSdk's call-back listeners
        linkUi(); //
        // ...then, with everything prepared and ready ...
        init(sessionId);  // init the library and register the agent to receive calls

	
        // caches the DOM elements we will need later during the call
        function cacheDom() {
            // extract native DOM elements - AgentSDK works with DOM elements directly
            share = $('#share')[0];
            // cache as jQuery objects for easier UI event handling
            $requestShare = $('#request-share');
            $end = $('#end');
            $mode = $('.mode');
            $annotationControl = $('.annotation-control');
        }

        // add click handlers that determine what happens when agent clicks buttons, etc
        function setClickHandlers() {
            // handle end call button being clicked
            $end.click(function(event) {
                // end the call
                AssistAgentSDK.endSupport();
            });

            // handle request screen share button being clicked
            $requestShare.click(function (event) {
                AssistAgentSDK.requestScreenShare();
            });

            // handle the change mode button being clicked
            $mode.change(function () {
                var $this = $(this);
                var mode = $this.find('[name=mode]:checked').val();
                console.log( '----------- Mode: ' + mode );
                // determine which mode to switch in to
                switch(mode) {
                    case 'control':
                        console.log('--------- controlSelected');
                        AssistAgentSDK.controlSelected();
                        break;
                    case 'draw':
                        console.log('--------- drawSelected');
                        AssistAgentSDK.drawSelected();
                        break;
                    case 'spotlight':
                        console.log('--------- spotlightSelected');
                        AssistAgentSDK.spotlightSelected();
                        break;
                }
            });

            // handle the annocation controls being set
            $annotationControl.submit(function (event) {
                // prevent normal form submission behaviour
                event.preventDefault();

                // extract the vars
                var $this = $(this);
                var color = $this.find('.color').val();
                var opacity = $this.find('.opacity').val();
                var width = $this.find('.width').val();

                // update the annotation draw style
                AssistAgentSDK.setAgentDrawStyle(color, opacity, width);
            });
        }

        // set all of the agentSdk's call-back listeners
        function bindAgentSdkCallbacks() {
            // Connection events
            AssistAgentSDK.setConnectionEstablishedCallback(function () {
                console.log('----------- setConnectionEstablishedCallback');
            });

            AssistAgentSDK.setConnectionLostCallback(function () {
                console.log('----------- setConnectionLostCallback');
            });

            AssistAgentSDK.setConnectionReestablishedCallback(function () {
                console.log('----------- setConnectionReestablishedCallback');
            });

            AssistAgentSDK.setConnectionRetryCallback(function (retryCount, retryTimeInMilliSeconds) {
                console.log('----------- setConnectionRetryCallback');
            });

            // Call event
            AssistAgentSDK.setConsumerEndedSupportCallback(function () {
                console.log('----------- setConsumerEndedSupportCallback');
                // clear the remove video view - optional
                $(remote).find('video').attr('src', '');
            });

            // Assist feature events
            AssistAgentSDK.setFormCallBack(function (form) {
                console.log('setFormCallBack');
            });

            AssistAgentSDK.setRemoteViewCallBack(function (x, y) {
                console.log('------------- setRemoteViewCallBack');
                console.log('x: ' + x + ', y: ' + y);
                // the div where the screen share is shown
                var $share = $('#share');

                var containerHeight = $share.height();
                var containerWidth = $share.width();
                var containerAspect = containerHeight / containerWidth;
                var remoteAspect = y / x;

                var height;
                var width;
                if (containerAspect < remoteAspect) {
                    // Container aspect is taller than the remote view aspect
                    height = Math.min(y, containerHeight);
                    width = height * (x / y);
                } else {
                    // Container aspect is wider than (or the same as) the remote view aspect
                    width = Math.min(x, containerWidth);
                    height = width * (y / x);
                }
                $share.height(height).width(width);
            });

            AssistAgentSDK.setScreenShareActiveCallback(function (active) {
                console.log('------------ ssetScreenShareActiveCallback active: ' + active);
            });

            AssistAgentSDK.setScreenShareRejectedCallback(function () {
                console.log('------------ setScreenShareRejectedCallback. Caller has rejected agents invitation to screen share');
            });

            AssistAgentSDK.setSnapshotCallBack(function (snapshot) {
                console.log('------------ setSnapshotCallBack');
            });
        }


        // links UI elements to their Agent SDK outlets
        function linkUi() {
            // Set screen share element
            AssistAgentSDK.setRemoteView(share);
        }

        // initialises using the Agent SDK CallManager
        function init(sessionId) {
             var gatewayUrl = 'https://192.168.4.21:8443';

             AssistAgentSDK.startSupport({
                 sessionToken: sessionId,
                 username: 'agent1',
                 password: 'none',
                 agentName: 'Agent',
                 agentPictureUrl: gatewayUrl + '/assistserver/img/avatar.png',
                 url: gatewayUrl,
                 correlationId: myCid
             });
        }

    }(sessionId);

</script>
</body>
</html>


